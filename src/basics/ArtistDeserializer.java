package basics;

import java.util.LinkedList;
import java.util.ArrayList;
import com.google.gson.annotations.SerializedName;


public class ArtistDeserializer {


    public ArrayList<Artist> artists;

    public class Artist{
        private String name, country;
        private ArrayList<Tags> tags;
        private Area area;

        @SerializedName("begin-area")
        private BeginArea beginArea;

    }

    public class Tags{
        String name;
        int count;
    }

    public class Area{
        String name,type;
    }

    public class BeginArea{
        String name,type;
    }
}

