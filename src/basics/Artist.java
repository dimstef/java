package basics;

import java.util.LinkedList;

public abstract class Artist {
    protected String Name, Country;
    protected String[] Cities;
    protected LinkedList<String> Aliases, Tags;
    
    public Artist(String Name, String Country, String[] Cities, LinkedList<String> Aliases, LinkedList<String> Tags){
        this.Name = Name;
        this.Country = Country;
        this.Cities = Cities;
        this.Aliases = Aliases;
        this.Tags = Tags;
    }
}
