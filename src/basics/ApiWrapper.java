package basics;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Scanner;

public class ApiWrapper {

    private static String getResponse(String u) throws IOException{

        URL url = new URL(u);
        url.openConnection();

        Scanner scanner = new Scanner(url.openStream());
        String response = scanner.useDelimiter("\\Z").next();
        scanner.close();

        return response;
    }

    public static ArtistDeserializer deserialize(String json){
        Gson gson = new Gson();
        return gson.fromJson(json, ArtistDeserializer.class);
    }

    public static void prettyPrint(ArtistDeserializer results){
        Gson json = new GsonBuilder().setPrettyPrinting().create();
        System.out.println(json.toJson(results));
    }

    private static ArrayList<ArtistDeserializer.Artist> genericArtistRequest(String u) throws IOException{
        try {
            String response = getResponse(u);

            ArtistDeserializer results = deserialize(response);
            prettyPrint(results);
            return results.artists;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static ArrayList<ArtistDeserializer.Artist> getArtistsByName(String name) throws IOException {
        String u = String.format("http://musicbrainz.org/ws/2/artist/?query=artist:%s&fmt=json",name);
        return genericArtistRequest(u);
    }

    public static ArrayList<ArtistDeserializer.Artist> getArtistsByCountry(String country) throws IOException{
        String u = String.format("http://musicbrainz.org/ws/2/artist/?query=country:%s&fmt=json",country);
        return genericArtistRequest(u);
    }

    public static ArrayList<ArtistDeserializer.Artist> getArtistsByTag(String tag) throws IOException{
        String u = String.format("http://musicbrainz.org/ws/2/artist/?query=tag:%s&fmt=json",tag);
        return genericArtistRequest(u);
    }

    public static ArrayList<ArtistDeserializer.Artist> getArtistsByGender(String gender) throws IOException{
        String u = String.format("http://musicbrainz.org/ws/2/artist/?query=gender:%s&fmt=json",gender);
        return genericArtistRequest(u);

    }

    public static ArrayList<ArtistDeserializer.Artist> getArtistsByAlias(String alias) throws IOException{
        String u = String.format("http://musicbrainz.org/ws/2/artist/?query=alias:%s&fmt=json",alias);
        return genericArtistRequest(u);
    }

    public static ArrayList<ArtistDeserializer.Artist> getArtistsByArea(String area) throws IOException{
        String u = String.format("http://musicbrainz.org/ws/2/artist/?query=area:%s&fmt=json",area);
        return genericArtistRequest(u);
    }

    public static ArrayList<ArtistDeserializer.Artist> getArtistsByBeginArea(String beginarea) throws IOException{
        String u = String.format("http://musicbrainz.org/ws/2/artist/?query=beginarea:%s&fmt=json",beginarea);
        return genericArtistRequest(u);
    }
}
