package basics;

import java.time.LocalDate;
import java.util.Arrays;

public class Release{
    protected String Title, Language, Status;
    protected String[] Format;
    protected LocalDate ReleaseDate;

    public Release(String Title, String Language, String[] Format, String Status, LocalDate ReleaseDate) {
        this.Title = Title;
        this.Language = Language;
        this.Format = Format;
        //this.TrackCount = TrackCount;
        this.Status = Status;
        this.ReleaseDate = ReleaseDate;
    }

    @Override
    public String toString() {
        System.out.println("Title: " + Title);
        System.out.println("Language: " + Language);
        System.out.println("Status: " + Status);
        System.out.println("Format: " + Arrays.toString(Format));
        System.out.println("Release date: " + ReleaseDate.toString());
        //return "{" + "Title=" + Title + ", Language=" + Language + ", Status=" + Status + ", Format=" + Arrays.toString(Format) + ", ReleaseDate=" + ReleaseDate + '}';
        return null;
    }
    
    public String getTitle() {
        return Title;
    }
    public void setTitle(String Title) {
        this.Title = Title;
    }

    public String getLanguage() {
        return Language;
    }
    public void setLanguage(String Language) {
        this.Language = Language;
    }

    public String[] getFormat() {
        return Format;
    }
    public void setFormat(String[] Format) {
        this.Format = Format;
    }

    /*public int getTrackCount() {
        return TrackCount;
    }
    public void setTrackCount(int TrackCount) {
        this.TrackCount = TrackCount;
    }*/
    
    public String getStatus() {
        return Status;
    }
    public void setStatus(String Status) {
        this.Status = Status;
    }

    public LocalDate getReleaseDate() {
        return ReleaseDate;
    }
    public void setReleaseDate(LocalDate ReleaseDate) {
        this.ReleaseDate = ReleaseDate;
    }
    
}
