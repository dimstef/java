package basics;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.Arrays;

public class Group extends Artist {
    private LocalDate BeginDate, EndDate;
    private LinkedList<String> Members;
    
    public Group(String Name, String Country, String[] Cities, LocalDate BeginDate, LinkedList<String> Aliases, LinkedList<String> Tags, LinkedList<String> Members){
        super(Name, Country, Cities, Aliases, Tags);
        this.Members=Members;
        this.BeginDate=BeginDate;
    }
    public Group(String Name, String Country, String[] Cities, LocalDate BeginDate, LocalDate EndDate, LinkedList<String> Aliases, LinkedList<String> Tags, LinkedList<String> Members){
        super(Name, Country, Cities, Aliases, Tags);
        this.Members=Members;
        this.BeginDate=BeginDate;
        this.EndDate=EndDate;
    }

    @Override
    public String toString() {
        //return "{" + "BeginDate=" + BeginDate + ", EndDate=" + EndDate + ", Members=" + Members.toString() + '}';
        System.out.println("Name: " + Name);
        System.out.println("Country: " + Country);
        //System.out.println("Gender: " + Gender);
        System.out.println("Cities: " + Arrays.toString(Cities));
        System.out.println("Begin date: " + BeginDate);
        if (EndDate!=null) System.out.println("Death date: " + EndDate);
        System.out.println("Members: " + Members.toString());
        System.out.println("Aliases: " + Aliases.toString());
        System.out.println("Tags: " + Tags.toString());
        //return "{" + "Name=" + Name + ", Country=" + Country + ", Gender=" + Gender + ", Cities=" + Arrays.toString(Cities) + ", BirthDate=" + BirthDate + ", DeathDate=" + DeathDate + ", Aliases=" + Aliases.toString() + ", Tags=" + Tags.toString() + '}';
        return null;
    }
   
    public LocalDate getBeginDate() {
        return BeginDate;
    }
    public void setBeginDate(LocalDate BeginDate) {
        this.BeginDate = BeginDate;
    }

    public LocalDate getEndDate() {
        return EndDate;
    }
    public void setEndDate(LocalDate EndDate) {
        this.EndDate = EndDate;
    } 
    
    public LinkedList<String> getMembers() {
        return Members;
    }
    public void setMembers(LinkedList<String> Members) {
        this.Members = Members;
    }

}    


