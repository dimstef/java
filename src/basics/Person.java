package basics;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.LinkedList;

public class Person extends Artist {
    //private String Name, Country, 
    String Gender;
    //String[] Cities;
    private LocalDate BirthDate, DeathDate;
    //private LinkedList<String> Aliases, Tags; 

    public Person(String Gender, String Name, String Country, String[] Cities, LocalDate BirthDate, LocalDate DeathDate, LinkedList<String> Aliases, LinkedList<String> Tags){
        //this.Name = Name;
        //this.Country = Country;
        //this.Cities = Cities;
        //this.BeginDate = BeginDate;
        //this.EndDate = EndDate;
        super(Name, Country, Cities, Aliases, Tags);
        this.BirthDate=BirthDate;      
        this.DeathDate=DeathDate;
        //this.Aliases = Aliases;
        //this.Tags = Tags;
        this.Gender=Gender;
        
    }
    public Person(String Gender, String Name, String Country, String[] Cities, LocalDate BirthDate, LinkedList<String> Aliases, LinkedList<String> Tags){
        /*this.Name = Name;
        this.Country = Country;
        this.Cities = Cities;*/
        super(Name, Country, Cities, Aliases, Tags);
        this.BirthDate=BirthDate;
        this.Gender=Gender;
        /*this.Aliases = Aliases;
        this.Tags = Tags;*/
        
    }
   /* protected Person(String Name, String Country, String[] Cities, LinkedList<String> Aliases, LinkedList<String> Tags){
        this.Name = Name;
        this.Country = Country;
        this.Cities = Cities;
        this.Aliases = Aliases;
        this.Tags = Tags;
        this.Gender=Gender;
    }*/

    @Override
    public String toString() {
        System.out.println("Name: " + Name);
        System.out.println("Country: " + Country);
        System.out.println("Gender: " + Gender);
        System.out.println("Cities: " + Arrays.toString(Cities));
        System.out.println("Birth date: " + BirthDate);
        if (DeathDate!=null) System.out.println("Death date: " + DeathDate);
        System.out.println("Aliases: " + Aliases.toString());
        System.out.println("Tags: " + Tags.toString());
        //return "{" + "Name=" + Name + ", Country=" + Country + ", Gender=" + Gender + ", Cities=" + Arrays.toString(Cities) + ", BirthDate=" + BirthDate + ", DeathDate=" + DeathDate + ", Aliases=" + Aliases.toString() + ", Tags=" + Tags.toString() + '}';
        return null;
    }

    public String getGender() {
        return Gender;
    }
    public void setGender(String Gender) {
        this.Gender = Gender;
    }
    
    public String getName() {
        return Name;
    }
    public void setName(String Name) {
        this.Name = Name;
    }

    public String getCountry() {
        return Country;
    }
    public void setCountry(String Country) {
        this.Country = Country;
    }

    public String[] getCities() {
        return Cities;
    }
    public void setCities(String[] Cities) {
        this.Cities = Cities;
    }

    public LinkedList getAliases() {
        return Aliases;
    }
    public void setAliases(LinkedList<String> Aliases) {
        this.Aliases = Aliases;
    }

    public LinkedList getTags() {
        return Tags;
    }
    public void setTags(LinkedList<String> Tags) {
        this.Tags = Tags;
    }
    
    public LocalDate getBirthDate() {
        return BirthDate;
    }
    public void setBirthDate(LocalDate BirthDate) {
        this.BirthDate = BirthDate;
    }

    public LocalDate getDeathDate() {
        return DeathDate;
    }
    public void setDeathDate(LocalDate DeathDate) {
        this.DeathDate = DeathDate;
    }   
}

