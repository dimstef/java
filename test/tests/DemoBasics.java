package tests;

import java.util.LinkedList;
import java.time.LocalDate;
import java.io.*;

import basics.*;

public class DemoBasics {
    public static void main(String[] args) throws IOException {
        //ApiWrapper.getArtistsByTag("rock");
        ApiWrapper.getArtistsByName("fred");
        //ApiWrapper.getArtistsByCountry("US");



        //LocalDate date=LocalDate.of(1970, 1, 10);
        //LocalDate.parse(date.toString(), DateTimeFormatter.ofPattern("dd/MM/uuuu"));
        //System.out.println(date);
        LinkedList<String> aliases, genre, members, tags;
        aliases=new LinkedList<String>();
        aliases.add("Demon");
        aliases.add("Space");
        aliases.add("Star");
        aliases.add("Cat");
        genre=new LinkedList<String>();
        genre.add("Rock");
        genre.add("Metal");
        members=new LinkedList<String>();
        members.add("Gene Simons");
        members.add("Paul Stanley");
        members.add("Ace Frehley");
        members.add("Peter Criss");
        tags=new LinkedList<String>();
        tags.add("80's");
        tags.add("Fire effects");
        String[] format = {"LP", "CD", "CS"};
        String[] cities={"New York", "Detroit", "Cleveland"};
       // int trackCount = 1;

        //Assigning data for Compilation object
        System.out.println(System.lineSeparator() + "Creating group...");
        Group kiss=new Group("Kiss", "USA", cities, LocalDate.of(1973, 1, 10) , aliases, genre, members);
        LinkedList artists = new LinkedList();
        artists.add(kiss);
        System.out.println("OK!" + System.lineSeparator());
        
        System.out.println(System.lineSeparator() + "Creating artist...");
        Person gene = new Person("Male", "Gene Simons", "USA", cities, LocalDate.of(1949, 8, 25), aliases, tags);
        artists.add(gene);
        System.out.println("OK!" + System.lineSeparator());
        
        System.out.println(System.lineSeparator() + "Creating album...");
        Album dynasty = new Album(gene,"Dynasty","English",format,"official", LocalDate.of(1979, 5, 23));
        System.out.println("OK!" + System.lineSeparator());
        
        System.out.println(System.lineSeparator() + "Creating compilation...");
        Compilation comp = new Compilation(artists, "80's hits", "English", format, "Unofficial", LocalDate.of(2019, 3, 30));
        System.out.println("OK!" + System.lineSeparator());

        System.out.println(System.lineSeparator() + "Creating release...");
        Release rel = new Release("Dynasty","English",format,"official", LocalDate.of(1979, 5, 23));
        System.out.println("OK!" + System.lineSeparator());
        
        /*System.out.println("title " + rel.getTitle());
        System.out.println("language " + rel.getLanguage());
        System.out.println("format " + rel.getFormat());
        System.out.println("status " + rel.getStatus());
        System.out.println("tracks " + rel.getTrackCount());
        System.out.println("release date " + rel.getReleaseDate());*/
        
        /*System.out.println("The demon's name is " + gene.getName());
        System.out.println("Gene's birth date: " + gene.getBirthDate());
        System.out.println("Begin date " + kiss.getBeginDate());
        System.out.println("Artists: " + comp.toString());*/
        System.out.println(System.lineSeparator() + "###Artist###");
        gene.toString();
        //System.lineSeparator();
        System.out.println(System.lineSeparator() + "###Group###");
        kiss.toString();
        //System.lineSeparator();
        System.out.println(System.lineSeparator() + "###Release###");
        rel.toString();
        System.out.println(System.lineSeparator() + "###Album###");
        dynasty.toString();
        //System.lineSeparator();
        System.out.println(System.lineSeparator() + "###Compilation###");
        comp.toString();
        //System.lineSeparator();
        System.out.println();
    }
}